//
//  SymptomAnalysis.swift
//  AEES
//
//  Created by Pieter van Loon on 23/03/2017.
//  Copyright © 2017 pietervanloonIT. All rights reserved.
//

import Foundation

typealias Symptoms = [Symptom]

typealias WordCombo = [String]

enum SymptomId: String {
    case backpain = "backpain"
    case chestpain = "chestpain"
    case dizzy = "dizzyness"
    case fainted = "fainting"
    case legHipNeckPain = "pain in leg, hip or neck"
    case nausea = "is nauseous"
}

struct Symptom: SeverityContainer {
    
    let id: SymptomId
    let wordCombos: [WordCombo]
    var severity: Double
    
    func appears(in sentence: String) -> Bool {
        let words = sentence.lowercased().components(separatedBy: CharacterSet(charactersIn: " ,"))
        for combo in wordCombos {
            var foundAll = true
            for word in combo {
                if !words.contains(word) {
                    foundAll = false
                    break
                }
            }
            if foundAll {
                return true
            }
        }
        return false
    }
    
}

protocol SeverityContainer {
    var severity: Double { get }
}

extension Array where Element: SeverityContainer {
    var totalSeverity: Double {
        return simpleSumTotalSeverity()
    }
    
    private func simpleSumTotalSeverity() -> Double {
        var total = 0.0
        for symptom in self {
            total += symptom.severity
        }
        return total
    }
    
}

let symptoms = [
    Symptom(id: .legHipNeckPain, wordCombos: [["leg", "hurts"], ["neck", "hurts"], ["hip", "hurts"], ["neckpain"], ["leg", "pain"], ["neck", "pain"], ["hip", "pain"]], severity: 0.1),
    Symptom(id: .backpain, wordCombos: [["back", "pain"], ["backpain"], ["back", "hurts"]], severity: 0.1),
    Symptom(id: .chestpain, wordCombos: [["chest", "pain"], ["chestpain"], ["chest", "hurts"]], severity: 0.3),
    Symptom(id: .dizzy, wordCombos: [["everything", "spinning"], ["dizzy"]], severity: 0.03),
    Symptom(id: .fainted, wordCombos: [["did", "faint"], ["fainted"], ["blacked", "out"]], severity: 0.1),
    Symptom(id: .nausea, wordCombos: [["feeling", "sick"], ["feeling", "ill"], ["nauseous"]], severity: 0.1),
]



