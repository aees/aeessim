//
//  ProtocolExtensions.swift
//  AEES
//
//  Created by Pieter van Loon on 09/03/2017.
//  Copyright © 2017 pietervanloonIT. All rights reserved.
//

import Foundation
import UIKit

extension Optional {
    var hasValue: Bool {
        return self != nil
    }
}

extension UIViewController {
    @IBAction func hide(_ sender: Any) {
        if navigationController?.popViewController(animated: true) != nil {
            dismiss(animated: true, completion: nil)
        }
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
