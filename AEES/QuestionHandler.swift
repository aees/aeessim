//
//  QuestionHandler.swift
//  AEES
//
//  Created by Pieter van Loon on 09/03/2017.
//  Copyright © 2017 pietervanloonIT. All rights reserved.
//

import Foundation

enum Question : String {
    case SirDidYouFall = "Sir, did you fall?"
    case didYouFall = "Did you fall?"
    case youAlright = "Are you alright?"
    case loudYouAlright = "ARE YOU ALRIGHT?"
    case sureYouAlright = "Are you sure you're alright?"
    case retryDidYouFall = "I'm sorry, I didn't get that. Did you fall?"
    case doYouNeedHelp = "Do you need help?"
    case askCallEmergency = "Shall I call your emergency contact?"
    case okDoYouHavePain = "Okay. Do you have pain somewhere?"
    case whatsWrong = "What's wrong? Do you have pain somewhere?"
    case didYouPressAccidentally = "Did you press the emergency button accidentally?"
    case pleaseRepeat = "Could you please repeat that?"
    case painSomewhereElse = "Do you also have pain somewhere else?"
    case tellMeWhereYouHavePain = "Tell me, where does it hurt?"

    var localized: String {
        return rawValue.localized
    }
}

enum Statement : String {
    case goodToHear = "Good to hear!"
    case callingNow = "Calling now!"
    case goodby = "Ok goodbye!"
    case dontKnowSymptom = "Sorry, I don't know that symptom"
    case okBackpain = "Ok so you have backpain!"
    case callingEmergencyContact = "I'm calling your emergency contact!"
    case callingEmergencyServices = "I'm calling the emergency services!"
    
    var localized: String {
        return rawValue.localized
    }
}

enum Answer {
    case yes
    case no
    case empty
    case unknown(String)
}

class QuestionHandler: QuestionDelegate {
    
    static var startedFromHelpButton = false
    
    var initialQuestion: Question {
        QuestionHandler.currentQuestion = QuestionHandler.startedFromHelpButton ? .whatsWrong : .SirDidYouFall
        return QuestionHandler.currentQuestion
    }
    
    static var currentQuestion: Question = .SirDidYouFall
    
    var foundSymptoms: [Symptom] = []
    
    func response(for answerString: String, to question: Question) -> QuestionResponse {
        let answer = parse(answer: answerString + " ")
        if question != .pleaseRepeat {
            QuestionHandler.currentQuestion = question
        }
        
        switch QuestionHandler.currentQuestion {
        case .SirDidYouFall:
            switch answer {
            case .yes:
                return .question(.youAlright)
            case .no:
                return .statement(.goodToHear)
            case .empty:
                return .question(.loudYouAlright)
            case .unknown(_):
                return .question(.pleaseRepeat)
            }
        case .youAlright:
            switch answer {
            case .yes:
                return .statement(.goodToHear)
            case .no, .empty:
                return .question(.doYouNeedHelp)
            case .unknown(_):
                return .question(.pleaseRepeat)
            }
        case .loudYouAlright:
            switch answer {
            case .yes:
                return .question(.didYouFall)
            case .no, .empty:
                return .question(.doYouNeedHelp)
            case .unknown(_):
                return .question(.pleaseRepeat)
            }
        case .didYouFall:
            switch answer {
            case .yes, .no, .empty:
                return .statement(.goodby)
            case .unknown(_):
                return .question(.pleaseRepeat)
            }
        case .doYouNeedHelp:
            switch answer {
            case .no:
                return .statement(.goodby)
            case .yes, .unknown(_), .empty:
                return .question(.okDoYouHavePain)
            }
        case .askCallEmergency:
            switch answer {
            case .yes, .empty:
                return .statement(.callingNow)
            case .no:
                return .question(.okDoYouHavePain)
            case .unknown(_):
                return .question(.pleaseRepeat)
            }
        case .okDoYouHavePain:
            switch answer {
            case .yes:
                return .question(.tellMeWhereYouHavePain)
            case .no:
                return .statement(.goodby)
            case .empty:
                return .statement(.callingEmergencyContact)
            case .unknown(_):
                return checkSymptom(in: answerString, for: QuestionHandler.currentQuestion)
            }
        case .painSomewhereElse:
            switch answer {
            case .yes:
                return .question(.tellMeWhereYouHavePain)
            case .empty, .no:
                return .statement(.callingEmergencyContact)
            case .unknown(_):
                return checkSymptom(in: answerString, for: QuestionHandler.currentQuestion)
            }
        case .whatsWrong, .tellMeWhereYouHavePain:
            return checkSymptom(in: answerString, for: QuestionHandler.currentQuestion)

        default:
            return .statement(.goodby)
        }
    }
    
    func checkSymptom(in answerString: String, for question: Question) -> QuestionResponse {
        
        for symptom in symptoms {
            if symptom.appears(in: answerString) {
                foundSymptoms.append(symptom)
                if foundSymptoms.totalSeverity >= 0.4 {
                    return .statement(.callingEmergencyServices)
                }
                if foundSymptoms.count == 2 {
                    return .statement(.callingEmergencyContact)
                }
                if foundSymptoms.count == 1 {
                    return .question(.painSomewhereElse)
                }
            }
        }
        return .question(.pleaseRepeat)
    }
    
    func parse(answer: String) -> Answer {
        if answer.isEmpty {
            return .empty
        } else if answer.lowercased().contains("yes ") {
            return .yes
        } else if answer.lowercased().contains("no ") {
            return .no
        } else {
            return .unknown(answer)
        }
    }
    
}


protocol QuestionDelegate {
    var initialQuestion: Question { get }
    
    func response(for answerString: String, to question: Question) -> QuestionResponse
}

enum QuestionResponse {
    case question(Question)
    case statement(Statement)
    case debug(String)
    
    var stringValue: String {
        switch self {
        case .question(let question):
            if question == .pleaseRepeat {
                return "\(question.localized)\n\(QuestionHandler.currentQuestion.localized)"
            }
            return question.localized
        case .statement(let statement):
            return statement.localized
        case .debug(let str):
            return str
        }
    }
}
