//
//  SimulationViewController.swift
//  AEES
//
//  Created by Pieter van Loon on 09/03/2017.
//  Copyright © 2017 pietervanloonIT. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

fileprivate let emptyQuestionString = "Questions of the AI will show here"

class SimulationViewController: UIViewController, AVSpeechSynthesizerDelegate, SFSpeechRecognizerDelegate, SFSpeechRecognitionTaskDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var voiceTextField: UITextField!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    let synthesizer = AVSpeechSynthesizer()
    let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    let isVoiceEnabled = true
    
    var currentResponse: QuestionResponse? {
        didSet {
            questionLabel.text = currentResponse?.stringValue ?? emptyQuestionString
            if let response = currentResponse, case .question(_) = response {
                answerButton.isEnabled = true
                questionLabel.textColor = UIColor.darkGray
            } else {
                answerButton.isEnabled = false
                questionLabel.textColor = UIColor.lightGray
            }
            
            if let response = currentResponse {
                let utterance = AVSpeechUtterance(string: response.stringValue)
                let voice = AVSpeechSynthesisVoice(language: "en-US")
                utterance.voice = voice
                synthesizer.speak(utterance)
            }
        }
    }
    
    var questionDelegate: QuestionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        synthesizer.delegate = self
        
        // Do any additional setup after loading the view.
        questionDelegate = QuestionHandler()
        currentResponse = nil
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        automaticallyAdjustsScrollViewInsets = false
        
        voiceTextField.returnKeyType = .send
        voiceTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(willShowKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        answerQuestion(textField)
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkStatus(SFSpeechRecognizer.authorizationStatus())
        
    }
    
    func willShowKeyboard(_ notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! CGRect).size
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
    }
    
    func willHideKeyboard(_ notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func startSimulation() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let delegate = self.questionDelegate {
                self.currentResponse = .question(delegate.initialQuestion)
            }
        }
    }
    
    // MARK: - Speech recognition
    
    func checkStatus(_ status:SFSpeechRecognizerAuthorizationStatus) {
        if status == .authorized {
            startSimulation()
        } else if status == .notDetermined {
            SFSpeechRecognizer.requestAuthorization(checkStatus)
        } else {
            let alert = UIAlertController(title: "Authorization denied", message: "This app needs permissions to recognize speech to function correctly. Please enable this permission in the settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                alert.addAction(UIAlertAction(title: "Settings", style: .default) { _ in
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                })
            }
            present(alert, animated: true, completion: nil)
        }
    }
    
    var timer: Timer?
    
    var didGetSpeech = false
    
    func restartTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: didGetSpeech ? 1 : 5, target: self, selector: #selector(stopSpeechRecognition), userInfo: nil, repeats: false)
//        timer = Timer(timeInterval: 5, target: self, selector: #selector(stopSpeechRecognition), userInfo: nil, repeats: false)
    }
    
    var didTriggerStop = false
    
    func stopSpeechRecognition() {
        didTriggerStop = true
        captureSession.stopRunning()
        speechRequest.endAudio()
        timer = nil
    }
    
    // MARK: Task delegate
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription) {
        didGetSpeech = true
        voiceTextField.text = transcription.formattedString
        if !didTriggerStop {
            restartTimer()
        }
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishRecognition recognitionResult: SFSpeechRecognitionResult) {
            answerQuestion(answer: recognitionResult.bestTranscription.formattedString)
            didTriggerStop = false
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully successfully: Bool) {
        if !successfully {
            answerQuestion(answer: "")
            didTriggerStop = false
        }
    }
    
    func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask) {
        task.finish()
    }
    
    func answerQuestion(answer: String) {
        if let delegate = questionDelegate, let response = currentResponse, case .question(let question) = response {
            currentResponse = delegate.response(for: answer, to: question)
            voiceTextField.text = nil
        }
    }
    
    // MARK: - SynthesizerDelegate
    var speechRequest: SFSpeechAudioBufferRecognitionRequest!
    var captureSession: AVCaptureSession!
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        synthesizer.stopSpeaking(at: .immediate)
        if let response = currentResponse {
            switch response {
            case .question(_):
                speechRequest?.endAudio()
                if isVoiceEnabled && speechRecognizer?.isAvailable ?? false {
                    speechRequest = SFSpeechAudioBufferRecognitionRequest()
                    
                    speechRecognizer?.recognitionTask(with: speechRequest, delegate: self)
                    captureSession?.stopRunning()
                    captureSession = AVCaptureSession()
                    let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
                    do {
                        let audioIn = try AVCaptureDeviceInput(device: audioDevice)
                        captureSession.addInput(audioIn)
                        let output = AVCaptureAudioDataOutput()
                        output.setSampleBufferDelegate(self, queue: DispatchQueue.main)
                        captureSession.addOutput(output)
                        output.connection(withMediaType: AVMediaTypeAudio)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: captureSession.startRunning)
                        captureSession.startRunning()
                        didGetSpeech = false
                        restartTimer()
                    } catch {
                        
                    }
                }
            case .statement(_), .debug(_):
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.currentResponse = nil
                }
            }
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        speechRequest.appendAudioSampleBuffer(sampleBuffer)
    }
    
    // MARK: - Actions
    
    @IBAction func cancelSimulation(_ sender: Any) {
        questionDelegate = nil
        speechRequest?.endAudio()
        synthesizer.stopSpeaking(at: .immediate)
        captureSession?.stopRunning()
        hide(sender)
    }
    
    @IBAction func restartSimulation(_ sender: Any) {
        questionDelegate = nil
        speechRequest?.endAudio()
        synthesizer.stopSpeaking(at: .immediate)
        captureSession?.stopRunning()
        currentResponse = nil
        voiceTextField.text = nil
        questionDelegate = QuestionHandler()
        startSimulation()
    }
    
    @IBAction func answerQuestion(_ sender: Any) {
        if let answer = voiceTextField.text {
            answerQuestion(answer: answer)
        }
    }
    
}
