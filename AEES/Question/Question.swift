////
//// Created by Pieter van Loon on 16/03/2017.
//// Copyright (c) 2017 pietervanloonIT. All rights reserved.
////
//
//import UIKit
//
//typealias Statement = String
//typealias Question = String
//
//enum QuestionResponse {
//    case question(Question)
//    case statement(Statement)
//}
//
//protocol QuestionDelegate {
//    var initialQuestion: Question { get }
//    
//    func response(for answer: String, to question: Question) -> QuestionResponse
//}
//
//extension QuestionResponse {
//    var stringValue: String {
//        switch self {
//        case .question(let question):
//            return question.localized
//        case .statement(let statement):
//            return statement.localized
//        }
//    }
//}
//
//struct QuestionObject {
//    let value: String
//    
//    var localizedString: String {
//        return value.localized
//    }
//    
//    init(_ value: String) {
//        self.value = value
//    }
//    
//    init(dict: [String: AnyObject]) {
//        self.init(dict["value"] as? String ?? "")
//    }
//    
////    static func ==(left: Question, right: Question) -> Bool {
////        return left.value == right.value
////    }
//    
//}
